<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %><%--
--%><%@page import="jaf.model2.util.JafUtil"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %><%--
--%><c:if  test="${empty local_polij_boot_db_time && empty local_polij_boot_controller_version}">
    <c:catch var="sessioneErrata">            
		<polij:useBean id="POLIJ_BOOT_DB_TIME" className="java.lang.String"/>
		<polij:useBean id="POLIJ_BOOT_CONTROLLER_VERSION" className="java.lang.String"/>
		<c:set var="local_polij_boot_db_time" scope="session"> ${POLIJ_BOOT_DB_TIME}</c:set>
		<c:set var="local_polij_boot_controller_version" scope="session"> ${POLIJ_BOOT_CONTROLLER_VERSION}</c:set>
	</c:catch>
    <c:if test="${not empty sessioneErrata}">
    	<c:set var="local_polij_boot_db_time" value=""> </c:set>
		<c:set var="local_polij_boot_controller_version" value=""> </c:set>
    </c:if>
</c:if>
<%
	String ver = System.getProperty("application.view.version");
	if(ver == null)
		ver= "";
%>
<table class="polifooter">
	<tfoot class="tfoot-tablePage">
    	<tr>
    		<td >
    			<div class="jaf-text-align-left">
	   				 <polij:servizioCorr /> ${local_polij_boot_controller_version}${empty local_polij_boot_controller_version ? " " :" / "}<%=ver%>
	   			</div>
    		</td>
    		<td>
    			<div class="jaf-text-align-center">
					<%=JafUtil.nullToEmpty(System.getProperty("polij_footer_descrizione_area"))%>
    			</div>
    		</td>
    		<td>
    			<div class="jaf-text-align-right" id="div-tfoot-tablePage-centerBar">
    				${local_polij_boot_db_time}
    			</div>
    		</td>
	   	</tr>
    </tfoot>
</table>