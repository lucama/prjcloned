<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %><%--
--%>
	<c:catch var="errorLinkReturn">
		<polij:useBean className="jaf.model2.authentication.Internationalizer" id="INTERNATIONALIZER"></polij:useBean>
	</c:catch>
	<div id="policontext">
		<nav class='nomeApp policontext-link' style="float: left; margin: auto 1em;" >
			<polij:servizioPrec/> <polij:servizioCorr />
		</nav>
		<div class="policontext-link">
			<nav>
				<%-- decommentare per utilizzare il link Manuale
				<c:catch var="errorInJsp">
					<span style="display:none" class="jaf-color-azzurro-scuro" >*</span><polij:linkManuale screenName="" />
				</c:catch>
				--%>
				<%-- decommentare per utilizzare il link richiedi assistenza
				<c:catch var="errorInJsp">
					<span style="display:none" class="jaf-color-azzurro-scuro" >*</span><polij:helpdeskAssistenza />
				</c:catch>
				--%>
				<%-- decommentare per utilizzare il link cambio lingua
				<c:catch var="errorInJsp">
					<span style="display:none" class="jaf-color-azzurro-scuro" >*</span><polij:lang />
				</c:catch>
				--%>
			</nav>
		</div>
		<div class="jaf-float-clear" ></div>
	</div>