<%@ taglib uri="http://www.ceda.polimi.it/taglibs/jaftemplate2-1.0" prefix="template" %><%--
--%><%@ page contentType="text/html;charset=ISO-8859-1" %><%--
--%><%
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	response.setDateHeader("Expires", 0); // Proxies.
%><%--
--%><!DOCTYPE html>
<html class="no-js">
	<head>
		<title><template:insert parameter="title"/></title>
		<template:insert parameter="head" defaultPath="/standard/head.jsp"/>
	</head>
	<body>
		<div id="jafBodyContainer">
			<div id="polijPageHeader">
				<template:insert parameter="banner" defaultPath="/standard/banner.jsp"/>
				<template:insert parameter="context" defaultPath="/standard/context.jsp"/>
			</div>
			<template:insert parameter="message" defaultPath="/standard/message.jsp"/>
			<template:insert parameter="body" />
			<template:insert parameter="footer" defaultPath="/standard/footer.jsp"/>
		</div>
	</body>
</html>