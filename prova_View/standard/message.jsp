<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %><%--
--%><%@page import="jaf.view2.GestioneErrore"%><%--
--%>
<c:if test="${jaf_message.class.name!='' && jaf_message.class.name!=null}">
	<polij:useBean id="INTERNATIONALIZER" className="jaf.model2.authentication.Internationalizer"/>
	<c:set var="errorType" value="${jaf_message.class.name}"/>
	<%
		String errorType = (String)pageContext.findAttribute("errorType");
		if(errorType!=null && !"".equals(errorType))
			pageContext.setAttribute("errorType", errorType.substring("jaf.controller2.core.".length()) );
	%>
	<c:set var="classMessage">
		<c:choose>
			<c:when test="${errorType == 'InfoMessage'}">jaf-ico-message-info</c:when>
			<c:when test="${errorType == 'WarningMessage'}">jaf-ico-message-warning</c:when>
			<c:when test="${errorType == 'ErrorMessage'}">jaf-ico-message-error</c:when>
			<c:otherwise></c:otherwise>
		</c:choose>
	</c:set>
	<div class='Message ${errorType}' >
		<polij:infoCard title="<%=INTERNATIONALIZER.getText(errorType)%>">
			<polij:row>
				<polij:element classElement="jaf-ico-message" width="2em">
					<span class="${classMessage}"></span>
				</polij:element>
				<polij:element classElement="">
					<c:forEach items="${jaf_message.messages}" var="messaggio">
						<%String messaggio = (String)pageContext.findAttribute("messaggio");%>
						<%=GestioneErrore.getMessaggio(messaggio,INTERNATIONALIZER)%>
						<br>
					</c:forEach>
				</polij:element>
			</polij:row>
		</polij:infoCard>
	</div>
</c:if>