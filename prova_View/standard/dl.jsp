<%@ taglib uri="http://www.ceda.polimi.it/taglibs/polij2-1.0" prefix="polij" %><%--
--%><polij:useBean id="multipart" className="jaf.common.service.Multipart" /><%--
--%><polij:useBean id="inline" className="java.lang.Boolean" /><%--
--%><%@page import="java.lang.Exception"%><%--
--%><%@page import="java.io.*"%><%--
--%><%@page import="jaf.model2.*"%><%--
--%><%byte[] contenuto_file = multipart.getAsBytes();
response.setContentType(multipart.getContentType());
response.setContentLength(contenuto_file.length);
response.setHeader("Filename", multipart.getFilename());
response.setHeader("Content-Disposition", (inline != null && inline ? "inline" : "attachment") + "; filename=" + multipart.getFilename());
response.getOutputStream().write(contenuto_file);

%>