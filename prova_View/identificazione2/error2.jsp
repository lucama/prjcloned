 <%@page import="jaf.view2.GestioneErrore"%>
<%@page import="jaf.model2.util.JafUtil"%>
<%@page import="java.util.Enumeration"%>
<%@page import="jaf.common.Configuration"%><%--
--%><%@ page contentType="text/html; charset=ISO-8859-1" isErrorPage="true"%><%--
--%><%@ taglib uri="http://www.ceda.polimi.it/taglibs/jaftemplate2-1.0" prefix="template" %><%--
--%><!DOCTYPE html>
<html>
	<head>
		<title>Error</title>
		<template:insert parameter="head" defaultPath="/standard/head.jsp" />
	</head>
	<body>
        <div id="jafBodyContainer">
		<div id="polijPageHeader">
            <template:insert parameter="banner" defaultPath="/standard/banner.jsp" />
           	<jsp:include page="/standard/context.jsp"/>
        </div>
		<div class="Message ErrorMessage"  id='mainErrore'>
			<table style='border-spacing: 0;border-collapse: collapse;'>
				<tbody>
					<tr>
						<td class="TitleInfoCard">Server Error</td>
					</tr>
				</tbody>
			</table>
			<table  class="BoxInfoCard" style='border-spacing: 0;border-collapse: collapse;'>
				<tbody>
					<tr>
						<td class="jaf-ico-message" rowspan="1" colspan="1">
							<span class="jaf-ico-message-error"></span>
						</td>
						<td class="" rowspan="1" colspan="1" >
						<%
                            String code =null;
                            String messaggio = null;
                            if(request.getAttribute("javax.servlet.error.status_code")!=null)
                                code = "" + request.getAttribute("javax.servlet.error.status_code");
							if (code != null && !code.equals(""))                                                           
								response.setStatus(Integer.parseInt(code));
                            
                        	java.lang.Throwable e = exception;
							if (e != null)
                            {
                                while (e.getCause() != null) e = e.getCause();
									messaggio = GestioneErrore.getMessaggio(e.getMessage(), null);
                            }
							   
                             if(Configuration.isSviluppoMode() || "true".equals(System.getProperty("show_stacktrace")))
                             {                                        
                                     java.io.ByteArrayOutputStream bout = new java.io.ByteArrayOutputStream();
                                     exception.printStackTrace(new java.io.PrintStream(bout));
                                     messaggio += "<br/>Stack trace:<br/><pre>" + bout.toString().replaceAll("[&<>\"'/]", "") + "</pre>";
                             }
                             else
                             {
                              	if(JafUtil.isEmpty(messaggio))
                              	{
	                                messaggio = "Il server ha incontrato un errore temporaneo e non ha completato la richiesta.";
	                                messaggio+= "<br/><br/>" ;
	                                messaggio+= "The server encountered a temporary error and could not complete your request.";
	
	                              if ("404".equals(code))
	                               {
	                                   messaggio ="Errore 404 - Pagina non trovata: " + request.getAttribute("javax.servlet.error.request_uri");
	                                   messaggio +="<br/><br/>(404 Error - File not found: " + request.getAttribute("javax.servlet.error.request_uri")+")";
	                               }
	                              else if("500".equals(code))
	                               {
	                                   messaggio ="Errore 500 Errore interno del server";
	                                   messaggio +="<br/><br/>(500 Error - Internal Server Error)";
	                               }
	                              else if("503".equals(code))
	                               {
	                                   messaggio ="Errore 503 Servizio non disponibile";
	                                   messaggio +="<br/><br/>(503 Error - Service Unavailable)";
	                               }
                              	}
                             }
                        out.println(messaggio);
						%>
						<br />
							<ul>
								<%
								 	String currentWF = (String) request.getParameter("jaf_currentWFID");
								 	if ((currentWF != null) && (!currentWF.trim().equals("")))
								 	{
								 %>
								<li>
									<a href="<%=request.getContextPath()%>/jaf/controller2/core/identificazione2/Error2.do?evn_gotosafepoint=evento&jaf_currentWFID=<%=currentWF.replaceAll("[^main0-9]", "")%>" class="">
										Ritorna all'ultima schermata valida / Return to the last screen valid
									</a>
								</li>
								<%}%>
								<li>
									<a href="<%=request.getContextPath()%>/jaf/controller2/core/identificazione2/Error2.do?evn_uscita=evento" class="">
										Logout
									</a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/>
		<jsp:include page="/standard/footer.jsp"/>
        </div>
	</body>
</html>