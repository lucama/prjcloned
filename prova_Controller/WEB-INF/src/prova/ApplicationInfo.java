package prova;

import prova.controller.Identificazione;
import jaf.controller2.core.StartingActivity;

import jaf.ant.ApplicationInfo2;

public class ApplicationInfo extends ApplicationInfo2
{
	public ApplicationInfo()
	{
		super();
	}

	public int getC_application()
	{
		return 24060;
	}

	public int getMajor()
	{
		//Se cambiate questo valore va cambiato anche
		// il parametro version
		// della tabella Applciations
		// dell'owner Login
		// sul DB
		return 0;
	}

	public int getMinor()
	{
		return 0;
	}

	public int getBuild()
	{
		return 0;
	}

	// Indica la prima Activity dell'applicazione
	public Class<? extends StartingActivity> getInitialActivity()
	{
		return Identificazione.class;
	}

	// Indica se il FrameWork deve eseguir e il controllo dei Grant
	// sugli eventi sollevati in una certa activity
	public boolean usesGrantProfiles()
	{
		return false;
	}
	// Indica il tipo di visibilitÓ dell'applicazione
	// AccessType.ANONYMOUS : l'applicazione NON richeide autentitcazione
	// AccessType.LOGIN : l'applicazione RICHIEDE autentitcazione
	public AccessType getDefaultAccessType()
	{
		return AccessType.LOGIN;
	}

	public void createNotes()
	{
		//Nelle applicazioni view per ora non vengono mostrate le note
		//Es. di come inserire una nota per gli utenti
		//addNote("DD/MM/AAAA", "0", "Primo rilascio.");
	}
}
