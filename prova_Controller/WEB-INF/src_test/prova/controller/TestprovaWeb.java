package prova.controller;

import jaf.testunit.JafTestWeb;

public class TestprovaWeb extends JafTestWeb
{
		protected void setUp() throws Exception {
			super.setUp();
			// dice che il corpo di ogni singolo test sar� eseguito entro una singola transazione
			getWorkspace().beginUserTransaction();
		}

		protected void tearDown() throws Exception {
			// effettua il rollback per 'buttare' via tutte le modifiche apportate dal test
			getWorkspace().rollbackUserTransaction();
			super.tearDown();
	}
}